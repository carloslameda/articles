
Articles App 

## Description

[Articles](https://gitlab.com/carloslameda/articles) Articles App starter repository.

Stack
Database: MongoDB
Server: Node.js + Nestjs
Client: React

## to run the entire app (server, client and mongodb) using docker-compose

```bash
$ docker-compose up --build
```

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Support

Nest is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers. If you'd like to join them, please [read more here](https://docs.nestjs.com/support).

## Stay in touch

- Author - [Carlos Lameda](https://www.linkedin.com/in/carlos-lameda-8085aa1b/)

## License

Nest is [MIT licensed](LICENSE).
