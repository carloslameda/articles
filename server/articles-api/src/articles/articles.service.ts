import { Model } from 'mongoose';
import {
  Injectable,
  BadRequestException,
  Logger,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Article, ArticleDocument } from './schemas/article.schema';
import { CreateArticleDto } from './dto/create-article.dto';
import { HttpService, HttpModule } from '@nestjs/axios';

@Injectable()
export class ArticlesService {
  private urlAPI = 'https://hn.algolia.com/api/v1/search_by_date?query=nodejs';

  private readonly logger = new Logger(ArticlesService.name);

  constructor(
    @InjectModel(Article.name) private articleModel: Model<ArticleDocument>,
    private readonly http: HttpService,
  ) {}

  async create(createArticleDto: CreateArticleDto): Promise<Article> {
    const createdArticle = new this.articleModel(createArticleDto);
    return createdArticle.save();
  }

  async findAll(): Promise<Article[]> {
    const result = await this.articleModel.find().exec();
    return result.sort(function (a, b) {
      if (a.createdAt > b.createdAt) {
        return -1;
      } else if (a.createdAt < b.createdAt) {
        return 1;
      }
      return 0;
    });
  }

  async digestArticles() {
    const result = await this.http
      .get(this.urlAPI)
      .toPromise()
      .then((res) => res.data.hits)
      .catch((err) =>
        this.logger.debug('Error fetching ' + this.urlAPI + ' - ' + err),
      );

    for (const key of Object.keys(result)) {
      this.logger.debug(key + ' -> ' + result[key].objectID);

      const article = await this.articleModel.findOne({
        id: result[key].objectID,
      });

      if (article != null) {
        continue;
      }

      const articleDto = new CreateArticleDto();
      articleDto.id = result[key].objectID;

      articleDto.author = result[key].author;
      articleDto.createdAt = new Date(result[key].created_at);
      articleDto.deleted = 'false';

      articleDto.url = result[key].url;
      if (articleDto.url == null) {
        articleDto.url = result[key].story_url;
      }

      articleDto.title = result[key].story_title;
      if (articleDto.title == null) {
        articleDto.title = result[key].title;
      }

      if (articleDto.title == null) {
        continue;
      }

      this.create(articleDto);
    }
  }

  async delete(localId: string) {
    const article = await this.articleModel.findOne({ id: localId });
    if (article != null) {
      article.deleted = 'true';
      return article.save();
    } else {
      throw new BadRequestException('Invalid article');
    }
  }
}
