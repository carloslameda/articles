import { Test, TestingModule } from '@nestjs/testing';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { DatabaseGenModule } from './databasegen.module';
import { ArticlesModule } from './articles.module';
import { Article } from './schemas/article.schema';
import { getConnectionToken } from '@nestjs/mongoose';
import { Connection, } from 'mongoose';

let connection: Connection;

describe('ArticlesController', () => {
  let articlesController: ArticlesController;
  let articlesService: ArticlesService;
  let moduleRef: TestingModule;
  const article1 = new Article();

  beforeEach(async () => {
    const aux = {};
    moduleRef = await Test.createTestingModule({
      imports: [DatabaseGenModule, ArticlesModule],
      controllers: [],
      providers: [],
    }).compile();

    articlesService = moduleRef.get<ArticlesService>(ArticlesService);
    articlesController = moduleRef.get<ArticlesController>(ArticlesController);

    article1.author = 'Carlos Lameda';
    article1.createdAt = new Date('2021-08-16T20:24:36.000+00:00');
    article1.deleted = 'False';
    article1.id = '28202481';
    article1.title = 'Test 1';
    article1.url = 'http://www.google.com';

  });

  afterEach(async () => {
    await moduleRef.close();
    connection = await moduleRef.get(getConnectionToken());
    connection.close();
  });

  describe('findAll', () => {
    it('should return an array of articles', async () => {
      const arrayResult = new Array<Article>();
      arrayResult.push(article1);

      const promise1 = new Promise<Article[]>((resolve, reject) => {
        setTimeout(() => {
          resolve(arrayResult);
        }, 0.1);
      });

      promise1.then((value) => {
        console.log(value);
        // expected output: "foo"
      });

      jest.spyOn(articlesService, 'findAll').mockImplementation(() => promise1);

      expect(await articlesController.findAll()).toBe(arrayResult);
    });
  });
});
