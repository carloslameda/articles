import {
  Body,
  Controller,
  Get,
  Post,
  Param,
  Delete,
  Logger,
} from '@nestjs/common';
import { ArticlesService } from './articles.service';
import { CreateArticleDto } from './dto/create-article.dto';
import { Article } from './schemas/article.schema';
import { Interval, Timeout } from '@nestjs/schedule';

const hourTimeInterval = 60 * 60 * 1000;
const oneTimeInterval = 1;

@Controller('articles')
export class ArticlesController {
  constructor(private readonly articlesService: ArticlesService) {}

  private readonly logger = new Logger(ArticlesController.name);

  @Post()
  async create(@Body() createArticleDto: CreateArticleDto) {
    await this.articlesService.create(createArticleDto);
  }

  @Get()
  async findAll(): Promise<Article[]> {
    return this.articlesService.findAll();
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return this.articlesService.delete(id);
  }

  @Interval(hourTimeInterval)
  handleInterval() {
    this.articlesService.digestArticles();
    this.logger.debug('Called every hour');
  }

  @Timeout(oneTimeInterval)
  handleTimeout() {
    this.articlesService.digestArticles();
    this.logger.debug('Called once after 1 milisecond');
  }
}
