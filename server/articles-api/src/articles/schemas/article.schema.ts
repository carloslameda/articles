import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ArticleDocument = Article & Document;

@Schema()
export class Article {
  @Prop({ required: true })
  id: string;

  @Prop({ required: true })
  title: string;

  @Prop({ required: true })
  createdAt: Date;

  @Prop()
  url: string;

  @Prop({ required: true })
  deleted: string;

  @Prop()
  author: string;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);
