export class CreateArticleDto {
  id: string;
  title: string;
  createdAt: Date;
  url: string;
  deleted: string;
  author: string;
}
