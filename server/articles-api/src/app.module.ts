import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ArticlesModule } from './articles/articles.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      useFactory: () => ({
        uri: 'mongodb://10.5.0.5:27017/test',
      }),
    }),
    ArticlesModule,
    ScheduleModule.forRoot(),
  ],
})
export class AppModule {}
