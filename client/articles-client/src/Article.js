import React, { Component } from "react";
import axios from "axios";


const URL_API = 'http://10.5.0.6:3000/articles/';

class Article extends Component {

    datesAreOnSameDay = (first, second) =>
        first.getFullYear() === second.getFullYear() &&
        first.getMonth() === second.getMonth() &&
        first.getDate() === second.getDate();

    datesAreOnSameYear = (first, second) =>
        first.getFullYear() === second.getFullYear()

    toDateFriendly(date) {

        let today = new Date();
        let yesterday = new Date(today);

        yesterday.setDate(yesterday.getDate() - 1);

        if (this.datesAreOnSameDay(date, today)) {
            return date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
        } else if (this.datesAreOnSameDay(date, yesterday)) {
            return "yesterday";
        } else if (this.datesAreOnSameYear(date, today)) {
            return date.toLocaleString('en-US', { month: 'long', day: 'numeric' });
        } else {
            return date.toLocaleString('en-US', { year: 'numeric', month: 'long', day: 'numeric' });
        }

    }

    createArticles(item) {


        return <tr key={item.key}>
            <td onClick={() => window.open(item.url, "_blank")}
                key={item.key}> {item.text} - <i>{item.author}</i> - </td>

            <td>{this.toDateFriendly(item.createdAt)}</td>

            <link
                rel="stylesheet"
                href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>


            <td><button class="fa fa-trash-o" onClick={() => this.delete(item.key)}></button></td>


        </tr>


    }

    constructor(props) {
        super(props);

        this.createArticles = this.createArticles.bind(this);
    }

    delete(key) {
        axios.delete(URL_API + key)
            .then(res => res.data
            ).catch(err => console.log('Error deleting ' + URL_API + key + " - " + err));

        this.props.delete(key);
    }

    openTab() {

    }

    render() {
        var articleEntries = this.props.entries;
        var listItems = articleEntries.map(this.createArticles);

        return (
            <ul className="theList">
                {listItems}
            </ul>
        );
    }
};

export default Article;
