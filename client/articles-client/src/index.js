import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import ArticlesList from "./ArticlesList.js";

var destination = document.querySelector("#container")

ReactDOM.render(
  <div>
    <div id="header">
      <h1>HN Feed</h1>
      <h2>We &lt;3 hacker news!</h2>
    </div>
    <ArticlesList />
  </div>,
  destination
);
