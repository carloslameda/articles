import React, { Component } from "react";
import Article from "./Article.js";
import axios from "axios";
import "./ArticlesList.css";

const URL_API = 'http://10.5.0.6:3000/articles';

class ArticlesList extends Component {

  constructor(props) {
    super(props);

    this.addItem = this.addItem.bind(this);
    this.deleteItem = this.deleteItem.bind(this);
  }

  state = {
    items: []
  };

  newItem = {};

  componentDidMount() {
    

    axios.get(URL_API)
      .then(res => {
        const itemsTmp = res.data;
        console.log(itemsTmp);
        for (let i = 0; i < itemsTmp.length; i++) {
          this.newItem = {
            text: itemsTmp[i].title,
            author: itemsTmp[i].author,
            createdAt: new Date(itemsTmp[i].createdAt),
            key: itemsTmp[i].id,
            deleted: itemsTmp[i].deleted,
            url: itemsTmp[i].url,
          };
          console.log(this.newItem);

          if (this.newItem.deleted === "true") {
            continue;
          }

          this.setState((prevState) => {
            return {
              items: prevState.items.concat(this.newItem)
            };
          });
        }

      }).catch(err => console.log('Error fetching ' + this.URL_API + " - " + err));


  }

  addItem(e) {
    if (this._inputElement.value !== "") {
      var newItem = {
        text: this._inputElement.value,
        author: "Carlos Lameda",
        createdAt: new Date(Date.now()),
        key: Date.now(),
        deleted: "false",
        url: "http://www.google.com"
      };

      this.setState((prevState) => {
        return {
          items: prevState.items.concat(newItem)
        };
      });

      this._inputElement.value = "";
    }

    console.log(this.state.items);

    e.preventDefault();
  }

  deleteItem(key) {
    var filteredItems = this.state.items.filter(function (item) {
      return (item.key !== key);
    });

    this.setState({
      items: filteredItems
    });
  }

  render() {
    return (
      <div className="articlesListMain">
        <div className="header">
        </div>
        <div className="body">
          <Article entries={this.state.items}
            delete={this.deleteItem} />
          <table>
            {this.items}
          </table>
        </div>
      </div>
    );
  }
}

export default ArticlesList;
